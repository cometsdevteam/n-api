require("babel-polyfill");
require("babel-register")({
    presets: ["env"],
});

const express = require("express");

const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

const cors = require("cors");
const app = express();
const config = require('./const/config');

app.engine("html", require("ejs").renderFile);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.set('jwt_key' , config.JWT_SECRET_KEY);

app.use(logger("dev"));
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
//GLOBAL DATA SETTING
setGlobalData();

const indexRouter = require("./routes/index");
app.use("/", indexRouter);

const userRouter = require("./routes/user");
app.use("/user", userRouter);

const chatRouter = require("./routes/chat");
app.use("/chat", chatRouter);

app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};
    res.status(err.status || 500);
    res.render("error");
});



function setGlobalData() {
    const mysql = require("mysql2/promise");
    global.config = require("./const/config");
    global.db = mysql.createPool(global.config.DB_CONFIG);
}
module.exports = app;
