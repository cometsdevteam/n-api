import firebase from "firebase-admin";
export function loadFB() {
    try {
        firebase.initializeApp({
            credential: firebase.credential.cert(require('../.serviceAccountKey')),
            databaseURL: "https://score-zone-8b3d8.firebaseio.com",
        });
    } catch (err) {
        console.error('Firebase initialization error', err);
    }
    return firebase;
}
