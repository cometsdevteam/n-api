require('babel-polyfill');
require('babel-register')({
    presets: ['env']
});
import {loadFB} from "../lib/firestore";
const firestore = loadFB().firestore();
const express = require("express");
const router = express.Router();


/* GET home page. */
router.get("/", async (req, res, next) => {
    const doc = await firestore.collection('test').doc('abc').get();
    res.send(doc.data());
});

module.exports = router;
