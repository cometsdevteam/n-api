const router = require("express").Router();
const jwt = require("jsonwebtoken");

/* GET users listing. */
router.get("/", async (req, res, next) => {
    const sql = "select * from user where no = 1 ";
    const user = await db.query(sql);
    res.send(JSON.stringify(user[0][0]));
});

router.post("/login", async (req, res, next) => {
    const body_data = req.body;

    const sql = "select * from user where email = ? and password = ? limit 1 ";
    const result = await db.query(sql, [body_data.email, body_data.password]);

    const user = result[0][0];
    if (user) {
        const secret = req.app.get("jwt_key");
        const user_data = {
            _id: user.id,
            level: user.level,
            email: user.email,
        };
        const jwt_opt = {
            expiresIn: "7d",
        };

        console.log(secret);
        const token = await new Promise((resolve, reject) => {
            jwt.sign(user_data, secret, jwt_opt, (err, token) => {
                if (err) reject(err);
                resolve(token);
            });
        });
        console.log("TOKEN IS ", token);
        res.send({
          result: true,
          token : token,
          code : 200,
          message: "로그인 성공",
      });
    } else {
        res.send({
            result: true,
            message: "회원 아이디나 비밀번호가 일치하지 않습니다.",
            code : -1
        });
    }

});

router.post("/register", async (req, res, next) => {
    const body_data = req.body;
    const data = {
        id: body_data.email.split("@")[0],
        email: body_data.email,
        password: body_data.password,
        nick: body_data.nick,
        status: "active",
        agent: "android",
        thumbUrl: "",
        level: 1,
        point: 0,
        regType: "basic",
    };
    const sql = "insert user set ? ";
    const result = await db.query(sql, [data]);
    console.log(result);
    res.send({
        result: true,
    });
});

module.exports = router;
